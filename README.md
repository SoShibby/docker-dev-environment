# Docker Dev Environment

## Forward SSH port to container
To be able to ssh into the "docker-dev-environment" container outside of the "docker-machine" virtualbox, you must forward the port. Open "Oracle VM Virtualbox Manager" (usually located at "C:\Program Files\Oracle\VirtualBox\VirtualBox.exe") and right click on your docker machine (usually named "default") and clck on "Settings". Next go to "Network" and expand "Advanced" and click on "Port Forwarding". Click on the plus sign to add a new rule and set the following to:

|Name|Value|
|---|---|
|Name|ssh container|
|Protocol|TCP|
|Host IP|0.0.0.0|
|Host Port|9000|
|Guest IP|   |
|Guest Port|9000|

Now you should be able to ssh into the container by using the port 9000. If you want to be able to ssh into the container from the internet you have make a port forwarding in your router.

## Permanent repo folder
Mount a folder from your Windows machine into the virtual box docker machine by opening the "Oracle VM Virtualbox Manager" (usually located at "C:\Program Files\Oracle\VirtualBox\VirtualBox.exe"). Right click on your docker machine (usually named "default") and click on "Settings". Next go to "Shared Folders" and add a new folder. Set the folder path to a folder on your Windows machine that you want to share with the docker machine (example: c:\repo). Set folder name to "repo". Check the box for "Auto-mount" and "Make Permanent". Set mount point to "repo2" (folder name and mount point should be different or you will get a strange error later).

Now we need to ssh into our docker machine by running:

```sh
docker-machine ssh
```
When inside the docker-machine run:

```sh
sudo mkdir -p /mnt/repo
sudo mount -t vboxsf -o defaults,uid=`id -u docker`,gid=`id -g docker` repo /mnt/repo
```
This will mount the repo folder from your Windows machine to /mnt/repo in your docker machine. So if you run:
```sh
ls /mnt/repo
```

You should see your files and folders. Now to make this permanent, so you don't lose your mount when you restart your virtual box machine, you have to ssh into you docker machine:
```sh
docker-machine ssh
```
and then run:
```sh
sudo vi /mnt/sda1/var/lib/boot2docker/bootlocal.sh
```

Now paste the following:

```sh
mkdir -p /mnt/repo
mount -t vboxsf -o defaults,uid=`id -u docker`,gid=`id -g docker` repo /mnt/repo
```

Now when you restart your docker machine you still have your repo folder and the content in it available.
