FROM ubuntu:16.04

########################
### Install Packages ###
########################

RUN apt update \
  `# Fix so we can add repositories to apt (add-apt-repository).` \
  && apt install -y software-properties-common \
  && add-apt-repository -y ppa:neovim-ppa/stable \
  && apt update \
  && apt install -y \
    `# Do not delete main-system-dirs` \
    safe-rm \
    `# Unzip, unrar etc.` \
    zip \
    unzip \
    rar \
    unrar \
    tar \
    `# Shells` \
    bash \
    fish \
    tmux \
    `# Get files from web` \
    wget \
    curl \
    `# Repository tools`\
    git \
    `# Neovim` \
    neovim \
    python-dev \
    python-pip \
    python3-dev \
    python3-pip \
    `# Symlink manager (for dotfiles)` \
    stow \
    `# SSH server so we can ssh into the container` \
    openssh-server \
    `# Install locales for 'Oh my fish' so fonts looks correct` \
    locales

# Install Docker CLI.
RUN export DOCKERVERSION=18.03.1-ce \
  && curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-$DOCKERVERSION.tgz \
  && tar xzvf docker-$DOCKERVERSION.tgz --strip 1 -C /usr/local/bin docker/docker \
  && rm docker-$DOCKERVERSION.tgz

# Install Docker-Compose.
RUN curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose

# Install Gitlab Lab CLI
RUN curl -s https://raw.githubusercontent.com/zaquestion/lab/master/install.sh | bash

# Install Neovim Package Manager (vim-plug).
RUN curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Install "Oh my fish!"
RUN curl -L https://get.oh-my.fish > ~/install-fish \
  && chmod +x ~/install-fish \
  && ~/install-fish -y --noninteractive \
  && rm ~/install-fish

# Install Nodejs and NPM
RUN curl -sL https://deb.nodesource.com/setup_13.x -o nodesource_setup.sh \
  && bash nodesource_setup.sh \
  && apt install -y nodejs

#####################
##### Setup SSH #####
#####################

RUN mkdir /var/run/sshd

# Uncomment this to enable root access (change the password below).
RUN echo 'root:test' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login.
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

RUN env NOTVISIBLE="in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

#####################
####### MISC ########
#####################

# Fix locale so fonts render correctly in fish shell.
RUN echo "LC_ALL=en_US.UTF-8" >> /etc/environment \
  && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
  && echo "LANG=en_US.UTF-8" > /etc/locale.conf \
  && locale-gen en_US.UTF-8

# Set fish-shell to the default shell.
RUN chsh -s `which fish`

# Install neovim plugins.
RUN nvim +'PlugInstall --sync' +qa

####################
##### Dotfiles #####
####################

COPY dotfiles /root/docker-dev-environment/dotfiles/
WORKDIR /root/docker-dev-environment/dotfiles

# Symlink all dotfiles
RUN stow -t ~ tmux
RUN stow -t ~ nvim
RUN stow -t ~ git

#######################
##### Source Code #####
#######################

COPY . /root/docker-dev-environment

# Set git remote. This is needed because we copy the git project from gitlab-runner and the remote url contains username and password for authenticating to gitlab.
RUN git remote set-url origin https://gitlab.com/SoShibby/docker-dev-environment.git

# Change branch to master in docker-dev-environment. So we can easily commit new changes without needing to first change branch.
RUN git checkout master

RUN apt install nano

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
