" Set leader key to ,
let mapleader = ","

call plug#begin()
  Plug 'scrooloose/nerdtree'
  Plug 'tyrannicaltoucan/vim-quantum'
  Plug 'mxw/vim-jsx'
  Plug 'pangloss/vim-javascript'
  Plug 'mhartington/oceanic-next'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'leafgarland/typescript-vim'
  Plug 'ternjs/tern_for_vim', { 'do' : 'npm install' }
  Plug 'w0rp/ale'
  Plug 'vim-scripts/argtextobj.vim'
  Plug 'christoomey/vim-tmux-navigator'
  Plug 'tpope/vim-fugitive'
  Plug 'mileszs/ack.vim'
call plug#end()

" NERDTREE
map <C-n> :NERDTreeToggle<CR>
nmap <C-m> :NERDTreeFind<CR>
set modifiable

" Show hidden files in file viewer
let NERDTreeShowHidden=1

" Remap Esc to jk
inoremap jk <ESC>

" THEME
syntax enable

set background=dark
set termguicolors
let g:quantum_italics=1

" Try to load the color scheme.
try
  colorscheme OceanicNext
catch 
endtry

set cursorline

" Use system clipbaord
set clipboard+=unnamedplus

let g:airline_theme='oceanicnext'

" CtrlP
let g:ctrlp_user_command = ['.git', 'cd %s & git ls-files -co --exclude-standard'] "Hide files in .gitignore
let g:ctrlp_show_hidden = 1                                                         "Show dotfiles

" Autocomplete
filetype plugin on
set omnifunc=syntaxcomplete#Complete

" Indent
filetype plugin indent on
" show existing tab with 2 spaces width
set tabstop=2
" when indenting with '>', use 2 spaces width
set shiftwidth=2
" On pressing tab, insert 2 spaces
set expandtab

" Show linenumbers
set relativenumber

" Don't add a EOL character at the end of the file.
set fileformats+=dos

" Match all cases when you search until you spcecify a case, then it will
" become case sensitive.
set smartcase

" Ack
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" Shortcut for navigating through buffers
nmap <leader>l :bnext<CR>
nmap <leader>h :bprevious<CR>
nmap <leader>bq :bp <BAR> bd #<CR>
nmap <leader>bl :ls<CR>
nmap <leader>ö :CtrlPBuffer<CR>

" Mapping key "-" to search. 
nmap - /
